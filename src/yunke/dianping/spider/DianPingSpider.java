package yunke.dianping.spider;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/2/23.
 */
public class DianPingSpider implements PageProcessor {

    Site site = Site.me().setSleepTime(3000).setTimeOut(60000).setRetryTimes(3)
                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");


    String type = null;
    int code = -0;
    public DianPingSpider(String type,int code){
        this.type = type;
        this.code = code;
    }
    HttpClientDownloader downloader = new HttpClientDownloader();
    Jedis jedis = new Jedis("localhost",6379);
    @Override
    public void process(Page page) {
        //从初始页面抓取行政区内的所有店铺地址
        if(page.getUrl().regex("g[0-9]+").match() && !page.getUrl().regex("g[0-9]+r1[0-9]+").match()){
            System.out.println(type);
            //抓取所有行政区的URL
            Elements eles = page.getHtml().getDocument().select("div[id=region-nav] a");
            //遍历行政区取出其中更深一层的URL
            for (int i = 0; i < eles.size(); i++) {
                System.out.println(type + ">>>" + eles.get(i).text());
                Document doc = null;
                try {
                    System.out.println(eles.get(i).attr("href"));
                    doc = Jsoup.connect(eles.get(i).attr("href"))
                            .header("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) " +
                                    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36").get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //取出商圈URL
                Elements eles2 = doc.select("div[id=region-nav-sub] a");
                for (int j = 1; j < eles2.size(); j++) {
                    System.out.println(type + ">>>" + eles.get(i).text() + ">>>" + eles2.get(j).text());
                    int count = 1;
                    for (int k = 1; k <= count; k++) {
                        try {
                            System.out.println("http://www.dianping.com" + eles2.get(j).attr("href") + "p" + k);
                            doc = Jsoup.connect("http://www.dianping.com" + eles2.get(j).attr("href")  + "p" + k)
                                    .header("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) " +
                                            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36").get();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try{
                            if(k == 1 && doc.select("div[class=page] a").size() > 0){
                                count = doc.select("div[class=page] a").size() - 2;
                                count = Integer.parseInt(doc.select("div[class=page] a").get(count).text());
                            }

                            //取出店铺URL
                            List<String> list = Util.getValues(doc.html(),"{\"s\":",",",20);

                            for (int l = 0; l < list.size(); l++) {
                                jedis.rpush("dianping" + code,"http://www.dianping.com/shop/" + list.get(l));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
            page.setSkip(true);
        }else if(page.getUrl().regex("shop/[0-9]+").match()){
            page.putField("name", page.getHtml().xpath("//h1[@class='shop-name']/text()"));
            page.putField("phone", page.getHtml().getDocument().select("span[itemprop=tel]").text());
            page.putField("address", page.getHtml().xpath("//*[@id=\"basic-info\"]/div[2]/span[2]/text()"));
        }else{
            page.setSkip(true);
        }

    }

    @Override
    public Site getSite() {
        return site;
    }


    public static void main(String[] args) throws IOException {
        Elements eles = Jsoup.connect("http://www.dianping.com/search/category/4/45/g147")
                .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36")
                .get().select("div[id=classfy] a");
        for (int i = 0; i < eles.size(); i++) {
            String link = "http://www.dianping.com" + eles.get(i).attr("href");
            Spider.create(new DianPingSpider(eles.get(i).text(),i))
                    .addUrl(link)
//                  .addPipeline(new JsonFilePipeline("d:/Data/dianping/"))
//                  .addPipeline(new ConsolePipeline())
                    .thread(1)
                    .run();
        }
    }
}
