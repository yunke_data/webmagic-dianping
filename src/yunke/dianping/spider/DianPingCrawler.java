package yunke.dianping.spider;

import org.apache.http.HttpHost;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.RedisScheduler;
import yunke.ganji.spider.JiaoYuPeiXunProcessor;
import yunke.util.spider.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/2/23.
 */
public class DianPingCrawler implements PageProcessor {

    Site site = Site.me().setSleepTime(3000).setTimeOut(60000).setRetryTimes(3)
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");


    static Jedis jedis = new Jedis("localhost",6379);
    String url = "";
    static List<String> list = new ArrayList<>();
    @Override
    public void process(Page page) {
        if(page.getUrl().toString().equals("https://www.dianping.com")){
            page.addTargetRequests(list);
            page.setSkip(true);
        }else{
            try {
                page.putField("name", page.getHtml().xpath("//h1[@class='shop-name']/text()").toString().trim());
                //page.putField("name", page.getHtml().getDocument().select("div[class=shop-name] h2").text());
                page.putField("phone", page.getHtml().getDocument().select("span[itemprop=tel]").text());
                page.putField("address", page.getHtml().getDocument().select("span[itemprop=street-address]").attr("title"));
                //page.putField("phone",page.getHtml().getDocument().select("div[class=phone] span[class=item]").text());

                if (page.getResultItems().get("name").equals("")) {
                    System.out.println("get page  : " + page.getUrl().toString());
                    page.setSkip(true);
                }
            }catch (Exception e){
                page.setSkip(true);
            }
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 22; i++) {
            list = jedis.lrange("dianping" + i,0,-1);
            System.out.println(list.size());
            int c = i;
            Spider.create(new DianPingCrawler())
                    .addUrl("https://www.dianping.com")
                    .addPipeline(new ConsolePipeline())
                    .addPipeline(new Pipeline() {
                        @Override
                        public void process(ResultItems items, Task task) {
                            String str = items.get("name") + "|" + items.get("phone") + "|" + items.get("address");
                            Util.method(str,"d:/广州健身"+c+".txt");
                        }
                    })
                    .thread(5)
                    .run();
        }

    }
}
