package yunke.lashou.spider;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.liebiao.spider.LiebiaoProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/5/10.
 */
public class LaShouProcessor implements PageProcessor{

    Site site = Site.me().setRetrySleepTime(5000).setTimeOut(60000).setRetryTimes(3)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
    @Override
    public Site getSite() {
        // TODO Auto-generated method stub
        return site;
    }

    int count = 0;
    public LaShouProcessor(int count){
        this.count = count;
    }
    @Override
    public void process(Page page) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList<>();
        list.add("http://guangzhou.liebiao.com/youyongguan/index1.html");
        for (int i = 0; i < list.size(); i++) {
            Spider.create(new LiebiaoProcessor(1))
                    .addUrl(list.get(i))
                    //.addPipeline(new JsonFilePipeline("d:/Data//"))
                    .addPipeline(new ConsolePipeline())
                    .thread(1)
                    .run();
        }
    }
}
