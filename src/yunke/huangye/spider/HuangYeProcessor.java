package yunke.huangye.spider;

import org.jsoup.select.Elements;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.ganji.spider.JiaoYuPeiXunProcessor;
import yunke.util.spider.Util;

import java.io.IOException;

/**
 * Created by admin on 2017/4/26.
 */
public class HuangYeProcessor implements PageProcessor{

    Site site = Site.me().setRetrySleepTime(5000).setTimeOut(60000).setRetryTimes(3)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
            //.setHttpProxy(new HttpHost("s1.proxy.mayidaili.com",8123));
    @Override
    public Site getSite() {
        // TODO Auto-generated method stub
        return site;
    }
    String url = "http://b2b.huangye88.com/guangzhou/jiaoyu/pn1/";
    int count = 1;
    @Override
    public void process(Page page) {
        // TODO Auto-generated method stub
        System.out.println("get page: " + page.getUrl().toString());
        Elements nameEles = page.getHtml().getDocument().select("h4");
        Elements phoneEles = page.getHtml().getDocument().select("span[itemprop=tel] a");
        for (int i = 0; i < nameEles.size(); i++) {
            String str = nameEles.get(i).text() + "|" + phoneEles.get(i).text();
            System.out.println(str);
            Util.method(str,"");
        }
        page.setSkip(true);
        count++;
        if(count < 126){
            page.addTargetRequest(url.replaceAll("pn[0-9]+","pn" + count));
        }
    }
    public static void main(String[] args) throws IOException {
        Spider.create(new HuangYeProcessor())
                .addUrl("http://b2b.huangye88.com/guangzhou/jiaoyu/pn1/")
                //.addPipeline(new JsonFilePipeline("d:/Data//"))
                .addPipeline(new ConsolePipeline())
                .thread(1)
                .start();
    }
}
