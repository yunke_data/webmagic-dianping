package yunke.baixing.spider;

import org.apache.commons.lang.StringUtils;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by admin on 2017/5/25.
 */
public class BaiXingProcessor implements PageProcessor{

    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000).setTimeOut(60000)
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
    Set<String> set = new HashSet<>();
    @Override
    public void process(Page page) {

        if (page.getUrl().toString().contains("page")) {
            System.out.println("get page : " + page.getUrl().toString());
            Elements eles = page.getHtml().getDocument().select("ul[class=list-ad-items] li");
            for (int i = 2; i < eles.size(); i++) {
                page.addTargetRequest(eles.get(i).select("div div a").first().attr("href"));
            }
            int pageNum = Integer.parseInt(page.getUrl().toString().substring(page.getUrl().toString().indexOf("page=")+5));
            pageNum++;
            page.addTargetRequest(page.getUrl().toString().replaceAll("page=[0-9]+","page=" + pageNum));
            page.setSkip(true);
        }else{
            page.putField("name",page.getHtml().getDocument().select("span[class=meta-公司名称]").text());
            page.putField("lianxiren",page.getHtml().getDocument().select("span[class=meta-联系人]").text());
            page.putField("phone",page.getHtml().getDocument().select("p[id=mobileNumber]").text());
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        Spider.create(new BaiXingProcessor())
                .addUrl("http://beijing.baixing.com/jiaoyupeixun/?page=1")
                .addPipeline(new ConsolePipeline())
                .addPipeline(new Pipeline() {
                    @Override
                    public void process(ResultItems items, Task task) {
                        String str = items.get("name") + "|" + items.get("lianxiren") + "|" + items.get("phone");
                        Util.method(str,"d:/北京培训.txt");
                    }
                })
                .thread(1)
                .run();
    }
}
