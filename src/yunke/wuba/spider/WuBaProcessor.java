package yunke.wuba.spider;

import org.apache.commons.lang.StringUtils;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

/**
 * Created by admin on 2017/5/15.
 */
public class WuBaProcessor implements PageProcessor {

    private Site site = Site.me().setTimeOut(6000).setRetryTimes(3).setSleepTime(10000).addHeader("Host","sz.58.com")
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

    @Override
    public void process(Page page) {
        if(page.getHtml().toString().contains("没有找到与")){
            return;
        }
        if(page.getUrl().regex("/pn[0-9]+").match()){
            page.addTargetRequests(page.getHtml().xpath("//div[@class='tdiv']/a").links().all());
            int pageNum = Integer.parseInt(StringUtils.substringBetween(page.getUrl().toString(),"/pn","/"));
            pageNum++;
            page.addTargetRequest(page.getUrl().replace("pn[0-9]+","pn" + pageNum).toString());
        }else{
            if(!page.getHtml().getDocument().title().contains("移民")){
                page.setSkip(true);
            }
            String str =  StringUtils.substringBetween(page.getHtml().toString(),"sname:'","'") + "|";
            str += page.getHtml().getDocument().select("a[rel=nofollow]").first().text() + "|";
            str += StringUtils.substringBetween(page.getHtml().toString(),"sphone:'","'") + "|";
            str += page.getHtml().getDocument().select("h1").text();
            page.putField("str", str);
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        Spider spider = new Spider(new WuBaProcessor());
        spider.addUrl("http://sz.58.com/yimin/pn2/?key=移民&cmcskey=移民公司");
        spider.thread(1);
        spider.addPipeline(new ConsolePipeline());
        spider.addPipeline(new Pipeline() {
            @Override
            public void process(ResultItems items, Task task) {
                if(items.get("str") != null){
                  Util.method(items.get("str"),"d:/深圳移民.txt");
                }
            }
        });
        spider.run();
    }
}
