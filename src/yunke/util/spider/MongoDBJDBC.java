package yunke.util.spider;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.List;

/**
 * Created by admin on 2017/5/28.
 */
public class MongoDBJDBC {

    public String addMongoDocuments(List<Document> documents){
        try{
            // 连接到 mongodb 服务
            MongoClient mongoClient = new MongoClient( "localhost" , 27017 );

            // 连接到数据库
            MongoDatabase mongoDatabase = mongoClient.getDatabase("yunke");
            System.out.println("Connect to database successfully");
            //选择文档集合
            MongoCollection<Document> collection = mongoDatabase.getCollection("baidunuomi");
            System.out.println("集合选择成功");
            //插入文档
            collection.insertMany(documents);
            System.out.println("文档插入成功");
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return "";
    }
}
