package yunke.util.spider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/5/10.
 */
public class Util {
    public static void method(String str,String path) {
        FileWriter fw = null;
        try {
            File f = new File(path);
            fw = new FileWriter(f, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter pw = new PrintWriter(fw);
        pw.println(str);
        pw.flush();
        try {
            fw.flush();
            pw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getValues(String s, String head, String footer, int times) {
        List<String> ret = new ArrayList<>();

        int idx1 = 0;
        while (true) {
            int idx2 = s.indexOf(head, idx1);
            if (idx2 == -1)
                break;
            idx2 += head.length();
            idx1 = s.indexOf(footer, idx2);
            if (idx1 == -1) {
                break;
            }
            String aXml = s.substring(idx2, idx1);
            ret.add(aXml);
            if (ret.size() >= times)
                return ret;
        }
        return ret;
    }
}
