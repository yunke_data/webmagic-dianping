package yunke.shunqi.spider;

import org.apache.commons.lang.StringUtils;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.Scheduler;
import yunke.util.spider.Util;

/**
 * Created by admin on 2017/5/15.
 */
public class ShunQiProcessor implements PageProcessor{

    private Site site = Site.me().setTimeOut(6000).setRetryTimes(3).setSleepTime(3000)
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
    int count = 1;
    @Override
    public void process(Page page) {

        if(page.getUrl().regex("-[0-9]+").match()){
            page.addTargetRequests(page.getHtml().xpath("//ul[@class='companylist']/li/div/h4/a").links().all());
            count++;
            if(count <= 20){
                page.addTargetRequest(page.getUrl().toString().replaceAll("pn[0-9]+","pn" + count));
            }
        }else{
            String str  = page.getHtml().getDocument().title() + "|";
            str += page.getHtml().regex("1[0-9]{10,10}");
            page.putField("str",str);
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        Spider spider = new Spider(new ShunQiProcessor());
        spider.addUrl("http://beijing.11467.com/dongcheng/pn1");
        spider.thread(1);
        spider.addPipeline(new ConsolePipeline());
        spider.addPipeline(new Pipeline() {
            @Override
            public void process(ResultItems items, Task task) {
                if(items.get("str") != null){
                    Util.method(items.get("str"),"d:/租掌柜.txt");
                }
            }
        });
        spider.run();
    }
}
