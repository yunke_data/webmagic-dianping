package yunke.baidunuomi.spider;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.MongoDBJDBC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/4/20.
 */
public class BaiDuNuoMiSpider implements PageProcessor{

    Site site = Site.me().setRetryTimes(3).setSleepTime(3000).setTimeOut(60000)
            .addHeader("Host","www.nuomi.com")
            .addHeader("Cookie","access_log=abff66124ed21947fb649030988d00b5; flag=101; BAIDUNUOMI_HOT_RANGE=100010000%3A1371_307; PHPSESSID=29mb2pnsg89kmp11rbf6nm7a51; channel_content=afdec7560000e94d0000000358f85e5f; channel_webapp=webapp; domainUrl=gz; gpsGot=0; visited=33011272%2C36986414%2C36954508%2C37037385%2C38056786; visitedRight=33011272%2C36986414%2C36954508%2C37037385%2C38056786%2C38076143%2C36979414%2C37803833%2C35251511%2C30601918; Hm_lvt_a028c07bf31ffce4b2d21dd85b0b8907=1490612063,1490958288,1492672098; Hm_lpvt_a028c07bf31ffce4b2d21dd85b0b8907=1492675250; BAIDUID=A3BBD157343C0B34794218D1EE7EA66E:FG=1; channel=www.baidu.com_index%7C%7C; areaCode=300110000")
            .addHeader("UserAgent","Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");

    String url = "https://www.nuomi.com/search/%E6%95%99%E8%82%B2/1/0/all-0-0-0-pd-0-0/1";
    int count = 1;
    @Override
    public void process(Page page) {
        if(page.getUrl().toString().contains("search")){
            page.addTargetRequests(page.getHtml().xpath("//ul[@class='itemlist clearfix']/li/a").links().all());
            count++;
            page.addTargetRequest(url.replaceAll("pd-0-0/1","pd-0-0/" + count));
            page.setSkip(true);
        }else{
            Document doc = null;
            String id = StringUtils.substringBetween(page.getHtml().toString(),"deal_id=","\"");
            try {
                doc = Jsoup.connect("https://www.nuomi.com/pcindex/main/shopchain?dealId=" + id)
                        .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0").header("Cookie","access_log=abff66124ed21947fb649030988d00b5; flag=101; BAIDUNUOMI_HOT_RANGE=100010000%3A1371_307; PHPSESSID=29mb2pnsg89kmp11rbf6nm7a51; channel_content=afdec7560000e94d0000000358f85e5f; channel_webapp=webapp; domainUrl=gz; gpsGot=0; visited=33011272%2C36986414%2C36954508%2C37037385%2C38056786; visitedRight=33011272%2C36986414%2C36954508%2C37037385%2C38056786%2C38076143%2C36979414%2C37803833%2C35251511%2C30601918; Hm_lvt_a028c07bf31ffce4b2d21dd85b0b8907=1490612063,1490958288,1492672098; Hm_lpvt_a028c07bf31ffce4b2d21dd85b0b8907=1492675250; BAIDUID=A3BBD157343C0B34794218D1EE7EA66E:FG=1; channel=www.baidu.com_index%7C%7C; areaCode=300110000")
                        .timeout(60000)
                        .get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            page.putField("name",page.getHtml().xpath("h2/text()"));
            page.putField("phone",StringUtils.substringBetween(doc.html(),"phone&quot;:&quot;","&quot;"));
            page.putField("address",decodeUnicode(StringUtils.substringBetween(doc.html(),"address&quot;:&quot;","&quot;")));

        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        MongoDBJDBC mdj = new MongoDBJDBC();
        List<org.bson.Document> documents = new ArrayList<>();
        Spider spider = new Spider(new BaiDuNuoMiSpider());
        spider.addUrl("https://www.nuomi.com/search/%E6%95%99%E8%82%B2/1/0/all-0-0-0-pd-0-0/1")
                .addPipeline(new Pipeline() {
                    @Override
                    public void process(ResultItems items, Task task) {
                        org.bson.Document document = new org.bson.Document("name",items.get("name").toString())
                                .append("phone",items.get("phone").toString())
                                .append("address",items.get("address").toString());
                        documents.add(document);
                        if(documents.size() == 10){
                            mdj.addMongoDocuments(documents);
                            spider.stop();
                        }
                    }
                })
                .addPipeline(new ConsolePipeline())
                .thread(5)
                .run();
    }

    public static String decodeUnicode(String theString) {

        char aChar;

        int len = theString.length();

        StringBuffer outBuffer = new StringBuffer(len);

        for (int x = 0; x < len;) {

            aChar = theString.charAt(x++);

            if (aChar == '\\') {

                aChar = theString.charAt(x++);

                if (aChar == 'u') {

                    // Read the xxxx

                    int value = 0;

                    for (int i = 0; i < 4; i++) {

                        aChar = theString.charAt(x++);

                        switch (aChar) {

                            case '0':

                            case '1':

                            case '2':

                            case '3':

                            case '4':

                            case '5':

                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';

                    else if (aChar == 'n')

                        aChar = '\n';

                    else if (aChar == 'f')

                        aChar = '\f';

                    outBuffer.append(aChar);

                }

            } else

                outBuffer.append(aChar);

        }

        return outBuffer.toString();

    }
}
