package yunke.bendibao.spider;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/5/8.
 */
public class BenDiBaoProcessor implements PageProcessor {

    Site site = Site.me().setTimeOut(60000).setRetryTimes(3).setSleepTime(1000)
            .addHeader("UserAgent","Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");
    int pageCount = 0;
    @Override
    public void process(Page page) {
        if(page.getUrl().toString().contains("list1")){
            Elements eles = page.getHtml().getDocument().select("ul[class=paginator] li");
            String moyeLink = eles.get(eles.size() - 1).select("a").attr("href");
            pageCount = Integer.parseInt(StringUtils.substringBetween(moyeLink,"list",".htm"));
        }
        String html = page.getHtml().getDocument().select("ul[class=catalist]").toString();
        Elements eles = Jsoup.parse(html).select("h3");
        String[] phones = StringUtils.substringsBetween(html,"电话：","<");
        for (int i = 0; i < phones.length; i++) {
            String str = eles.get(i).text() + "|" + phones[i];
            System.out.println(str);
            //Util.method(str,"d:/北京运动健身.txt");
        }
        int count = Integer.parseInt(StringUtils.substringBetween(page.getUrl().toString(),"list",".htm")) + 1;
        if(count > pageCount){
            return;
        }
        page.addTargetRequest(page.getUrl().toString().replaceAll("list[0-9]+","list" + count));

    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("http://bj.bendibao.com/wangdian/jianshenzhongxin/");
        list.add("http://bj.bendibao.com/wangdian/youyongguan/");
        list.add("http://bj.bendibao.com/wangdian/yumaoqiuguan/");
        list.add("http://bj.bendibao.com/wangdian/pingpangqiuguan/");
        list.add("http://bj.bendibao.com/wangdian/yujia/");
        list.add("http://bj.bendibao.com/wangdian/wangqiuchang/");
        list.add("http://bj.bendibao.com/wangdian/lanqiuchang/");
        list.add("http://bj.bendibao.com/wangdian/zuqiuchang/");
        list.add("http://bj.bendibao.com/wangdian/gaoerfuqiuchang/");
        list.add("http://bj.bendibao.com/wangdian/baolingqiuguan/");
        list.add("http://bj.bendibao.com/wangdian/huaxuechang/");
        list.add("http://bj.bendibao.com/wangdian/wushuchuangguan/");
        list.add("http://bj.bendibao.com/wangdian/tiyuchangguan/");
        for (int i = 0; i < list.size(); i++) {
            Spider.create(new BenDiBaoProcessor())
                    .addUrl(list.get(i) + "list1.htm")
                    .thread(1)
                    .run();
        }
    }
}
