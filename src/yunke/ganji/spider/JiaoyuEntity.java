package yunke.ganji.spider;

/**
 * Created by admin on 2017/4/21.
 */
public class JiaoyuEntity {

    private String title;
    private String name;
    private String phone;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
