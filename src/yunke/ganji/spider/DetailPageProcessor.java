package yunke.ganji.spider;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

import java.io.IOException;

/**
 * Created by admin on 2017/4/24.
 */
public class DetailPageProcessor implements PageProcessor{

    Site site = Site.me().setRetrySleepTime(5000).setTimeOut(60000).setRetryTimes(3)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")
            .setHttpProxy(new HttpHost("s1.proxy.mayidaili.com",8123));
    @Override
    public Site getSite() {
        // TODO Auto-generated method stub
        return site;
    }
    String url = "http://gz.ganji.com/yundongjianshen/o41/";
    int count = 41;
    @Override
    public void process(Page page) {
        // TODO Auto-generated method stub

        if(page.getUrl().regex("o[0-9]+").match()){
            page.addTargetRequests(page.getHtml().xpath("//li[@class='list-noimg']/div/p/a").links().all());
            count++;
            page.addTargetRequest(url.replaceAll("o41", "o" + count));
            System.out.println("get page " + page.getUrl().toString());
            page.setSkip(true);
        }else{
            page.putField("dianpuName", page.getHtml().xpath("//li[@class='fb']/text()"));
            page.putField("lianxiren", page.getHtml().xpath("//*[@id='dzcontactus']/div/ul/li[4]/p/text()"));
            page.putField("phone", StringUtils.substringBetween(page.getHtml().toString(), "@phone=", "@"));
        }
    }

    public static void main(String[] args) throws IOException {
        Spider.create(new DetailPageProcessor())
                .addUrl("http://gz.ganji.com/yundongjianshen/o41/")
                .addPipeline(new JsonFilePipeline("d:/Data/运动健身/"))
                .addPipeline(new ConsolePipeline())
                .thread(2)
                .start();
    }
}
