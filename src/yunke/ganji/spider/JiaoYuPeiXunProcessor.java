package yunke.ganji.spider;

import org.jsoup.select.Elements;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/4/21.
 */
public class JiaoYuPeiXunProcessor implements PageProcessor{

    Site site = Site.me().setRetrySleepTime(5000).setTimeOut(60000).setRetryTimes(3)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

    int count = 1;
    @Override
    public void process(Page page) {
        System.out.println("get page: " + page.getUrl().toString());
        Elements eles = page.getHtml().getDocument().select("li[class=list-img clearfix]");
        for (int i = 0; i < eles.size(); i++) {
            String str = eles.get(i).select("p[class=t clearfix]").text() + "|" + eles.get(i)
                    .select("div").get(eles.get(i).select("div").size() - 1)
                    .select("a").attr("data-phone");
            Util.method(str,"");
            System.out.println(str);
        }
        count++;
        if(count > 33 || page.getHtml().toString().contains("适当删减或更改搜索关键词")){
            page.setSkip(true);
            return;
        }
        page.addTargetRequest(page.getUrl().toString().replaceAll("o[0-9]+","o" + count));
        page.setSkip(true);
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("http://gz.ganji.com/qinzijiaoyu/o1/");
        list.add("http://gz.ganji.com/zhilikaifa/o1/");
        list.add("http://gz.ganji.com/youeryuan/o1/");
        list.add("http://gz.ganji.com/taijiao/o1/");
        list.add("http://gz.ganji.com/xueqianjiaoyu/o1/");
        list.add("http://gz.ganji.com/qitayingyouer/o1/");
        list.add("http://gz.ganji.com/itpeixun/o1/");
        list.add("http://gz.ganji.com/renzhengpeixun/o1/");
        list.add("http://gz.ganji.com/pxwangzhansheji/o1/");
        list.add("http://gz.ganji.com/pxwangluoyingxiao/o1/");
        list.add("http://gz.ganji.com/pxwangluoanquan/o1/");
        list.add("http://gz.ganji.com/pxwangluogongchengshi/o1/");
        list.add("http://gz.ganji.com/pxruanjiankaifa/o1/");
        list.add("http://gz.ganji.com/pxruanjianceshi/o1/");
        list.add("http://gz.ganji.com/pxdiannaoweixiu/o1/");
        list.add("http://gz.ganji.com/pxsikerenzheng/o1/");

        for (int i = 0; i < list.size(); i++) {
            Spider.create(new JiaoYuPeiXunProcessor())
                    .addUrl(list.get(i))
                    .addPipeline(new ConsolePipeline())
                    .thread(1)
                    .run();
        }

    }


}
