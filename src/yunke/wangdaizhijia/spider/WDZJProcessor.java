package yunke.wangdaizhijia.spider;

import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

import java.util.List;

/**
 * Created by admin on 2017/5/18.
 */
public class WDZJProcessor implements PageProcessor{

    private Site site = Site.me().setTimeOut(6000).setRetryTimes(3).setSleepTime(3000)
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

    String url = "http://www.wdzj.com/dangan/";
    @Override
    public void process(Page page) {
        if(page.getUrl().toString().equals("http://www.wdzj.com/wdzj/html/json/dangan_search.json")){
            List<String> list = Util.getValues(page.getJson().toString(),"\"platPin\":\"","\"",5000);
            for (int i = 0; i < list.size(); i++) {
                page.addTargetRequest("http://www.wdzj.com/dangan/" + list.get(i));
            }
            page.setSkip(true);
        }else{
            page.putField("name", page.getHtml().getDocument().select("h1").get(1).text());
            page.putField("phone", page.getHtml().regex("座机电话：[0-9]{3,4}-[0-9]{7,8}").replace("座机电话：",""));
        }

    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        Spider.create(new WDZJProcessor())
                .addUrl("http://www.wdzj.com/wdzj/html/json/dangan_search.json")
                .addPipeline(new Pipeline() {
                    @Override
                    public void process(ResultItems resultItems, Task task) {
                        Util.method(resultItems.get("name") + "|" + resultItems.get("phone"),"d:/互联网金融.txt");
                    }
                })
                .addPipeline(new ConsolePipeline())
                .thread(5)
                .run();
    }
}
