package yunke.liebiao.spider;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import yunke.util.spider.Util;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by admin on 2017/4/26.
 */
public class LiebiaoProcessor implements PageProcessor{
    Site site = Site.me().setRetrySleepTime(5000).setTimeOut(60000).setRetryTimes(3)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
    //.setHttpProxy(new HttpHost("s1.proxy.mayidaili.com",8123));
    @Override
    public Site getSite() {
        // TODO Auto-generated method stub
        return site;
    }
    Set<String> set = new HashSet<>();
    int count = 0;
    public LiebiaoProcessor(int count){
        this.count = count;
    }
    @Override
    public void process(Page page) {
        // TODO Auto-generated method stub
        try{
            int pageNum = Integer.parseInt(StringUtils.substringBetween(page.getUrl().toString(),"index",".html"));
            int pageNow = Integer.parseInt(page.getHtml().getDocument().select("li[class=active]").text());
            if(pageNow == pageNum){
                System.out.println("get page: " + page.getUrl().toString());
                Elements liEles = page.getHtml().getDocument().select("div[class=info-list] ul li");
//                Elements nameEles = page.getHtml().getDocument().select("h2[class=name] a");
//                Elements lianxirenEles = page.getHtml().getDocument().select("div[class=detail] span[class=address]");
//                Elements phoneEles = page.getHtml().getDocument().select("p[class=contact_phone] span");
                for (int i = 2; i < liEles.size(); i++) {
                    String str = liEles.get(i).select("h2[class=name] a").text() + "|"
                            + liEles.get(i).select("span[class=address]").text() + "|"
                            + liEles.get(i).select("p[class=contact_phone] span").text();
                    if(set.add(liEles.get(i).select("p[class=contact_phone] span").text())){
                        System.out.println(str);
                        Util.method(str,"d:/广州户外运动.txt");
                    }
                }
            }else{
                return;
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        count++;
        page.addTargetRequest(page.getUrl().toString().replaceAll("index[0-9]+","index" + count));
        page.setSkip(true);
    }
    public static void main(String[] args) throws IOException {

        //教育培训
//        List<String> list = new ArrayList<>();
//        Elements eles = Jsoup.connect("http://guangzhou.liebiao.com/peixun/").get().select("div[class=subcate] a");
//        for (int i = 0; i < eles.size(); i++) {
//            Spider.create(new LiebiaoProcessor(1))
//                    .addUrl("http://guangzhou.liebiao.com" + eles.get(i).attr("href") + "index1.html")
//                    //.addPipeline(new JsonFilePipeline("d:/Data//"))
//                    .addPipeline(new ConsolePipeline())
//                    .thread(1)
//                    .run();
//        }

        //运动健身
        Elements eles = Jsoup.connect("http://guangzhou.liebiao.com/huwai/")
                .get().select("div[class=cond-con] dl").get(0).select("a");
        for (int i = 1; i < eles.size(); i++) {
            Spider.create(new LiebiaoProcessor(1))
                    .addUrl(eles.get(i).attr("href") + "index1.html")
                    //.addPipeline(new JsonFilePipeline("d:/Data//"))
                    .addPipeline(new ConsolePipeline())
                    .thread(1)
                    .run();
        }
    }
}
